#!/usr/bin/env python2

import argparse
import ConfigParser
import uuid
import sys
import os
from faxlib.callcontroller import CallController
from faxlib.models import Fax


def parse_config_sources():
    init_parser = argparse.ArgumentParser(description=__doc__, add_help=False)
    init_parser.add_argument('-c', '--config',
                             help='Specify config file',
                             metavar='FILE')

    args, passthrough_args = init_parser.parse_known_args()
    base_config = {}

    # Parse config.ini or alternative provided by --config
    config = ConfigParser.RawConfigParser()
    if args.config:
        config.read([args.config])
    else:
        config.read('./config.ini')
    # Update settings with config.ini
    base_config.update(dict(config.items('Defaults')))
    # gets boolean options from config
    for option in config.items('Booleans'):
        base_config.update(
            {option[0]: config.getboolean('Booleans', option[0])})

    # Parse config overrides and required cli arguments
    parser = argparse.ArgumentParser(
        description=('Fax test 2.0 All arguments can be set in config.ini. '
                     'An alternate config can be specified with -c'),
        formatter_class=lambda prog: argparse.HelpFormatter(
            prog, max_help_position=100, width=200),
        parents=[init_parser])
    parser.set_defaults(**base_config)
    required = parser.add_argument_group('required arguments')
    required.add_argument('-u', '--auth',
                          metavar='username:password',
                          help='Freeswitch authentication')
    required.add_argument('-r', '--host',
                          help='Freeswitch server')
    required.add_argument('-s', '--source',
                          metavar='DID',
                          help='Source number')
    required.add_argument('-d', '--destination',
                          metavar='DID(s)',
                          help=('DIDs, DID or file of newline separated DIDs'))
    parser.add_argument('-e', '--ecm',
                        action='store_true',
                        help='Activate Error Correction')
    parser.add_argument('-V', '--v17',
                        action='store_true',
                        help='Send as T.38 v17')
    parser.add_argument('-Nt', '--no-t38',
                        dest='t38',
                        action='store_false',
                        help='Disable T.38')
    parser.add_argument('-Nr', '--no-t38-request',
                        dest='t38_request',
                        action='store_true',
                        help='Disable T.38 request')
    parser.add_argument('-C', '--codec',
                        metavar='PCMU',
                        help='Initial audio codec(eg. PCMU,PCMA,G729)')
    parser.add_argument('-b', '--bigfax',
                        action='store_true',
                        help='Send bigger fax')
    
    args = parser.parse_args(passthrough_args)
    for arg in vars(args):
        if not vars(args)[arg]:
            if arg in ['auth', 'host', 'destination', 'source']:
                sys.exit('auth, host, source and destination are required '
                         'arguments. The can also be added to config.ini')
    return args


def is_bulk(sd):
    if os.path.isfile(sd):
        return True
    else:
        return False


def get_settings():
    args = parse_config_sources()
    variables = vars(args)
    variables['bulk_destination'] = is_bulk(args.destination)
    variables['bulk_source'] = is_bulk(args.source)
    """size variable is not currently used. only one file exists at the moment.
    when a second file is added this should be converted from size to
    variables['file'] = 'testfax_stdres.tif'"""
    if variables['bigfax']:
        variables['size'] = 'large'
    else:
        variables['size'] = 'small'
    variables['oid'] = str(uuid.uuid1())
    return variables


def connector(ctl, server, max_retries):
    # Make sure connection is active before each call attempt.
    try:
        server.system.methodExist('freeswitch.api')
        return server
    except Exception:
        try:
            server = ctl.connect(max_retries)
            return server
        except RuntimeError as e:
            sys.exit(e)


def main():
    exec ""
    # add all variables to scope
    locals().update(get_settings())

    # instantiate freeswitch controller
    ctl = CallController(auth, host, port)

    # connect to server. exit code 1 and print error if connection fails.
    server = connector(ctl, None, connection_attempts)
    if server is None:
        sys.exit("Failed to connect to server.")

    # Call generation
    # Iterate through multiple sources using the same destination.
    if bulk_source:
        with open(source) as dids:
            for did in [d.rstrip('\n') for d in dids if d is not None]:
                # Make sure connection is active before each call attempt.
                server = connector(ctl, server, connection_attempts)

                call = Fax(source=did,
                           destination=destination,
                           prefix=prefix)
                try:
                    print "Calling from {0} to {1} with an oid of {2}".format(
                        did, destination, call.oid)
                    ctl.send_call(server, call)
                except KeyboardInterrupt as e:
                    server.freeswitch.api('bgapi', 'uuid_kill {0}'.format(
                        call.oid))
                    sys.exit(e)
                except Exception as e:
                    print e

    # Iterate through destinations using the same source.
    elif bulk_destination:
        with open(destination) as dids:
            for did in [d.rstrip('\n') for d in dids if d is not None]:
                # Make sure connection is active before each call attempt.
                server = connector(ctl, server, connection_attempts)

                call = Fax(source=source,
                           destination=did,
                           prefix=prefix)

                try:
                    print "Calling from {0} to {1} with an oid of {2}".format(
                        source, did, call.oid)
                    ctl.send_call(server, call)
                except KeyboardInterrupt as e:
                    server.freeswitch.api('bgapi', 'uuid_kill {0}'.format(
                        call.oid))
                    sys.exit(e)
                except Exception as e:
                    print e

    # send one call with a defined source and destination.
    else:
        call = Fax(source=source,
                   destination=destination,
                   prefix=prefix)
        try:
            ctl.send_call(server, call)
        except KeyboardInterrupt as e:
            server.freeswitch.api('bgapi', 'uuid_kill {0}'.format(call.oid))
            sys.exit(e)
        except Exception as e:
            print "Call failed.", e


if __name__ == '__main__':
    sys.exit(main())
