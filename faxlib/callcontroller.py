# -*- coding: utf-8 -*-

import socket
from xmlrpclib import ServerProxy
import re
import time


class CallController(object):
    """
    Base controller for interacting with a Freeswitch application.
    Attributes:
        host    (string): ip address or hostname
        port    (int)
    """
    def __init__(self, auth, host, port=8181):
        super(CallController, self).__init__()
        self.auth = auth
        self.host = host
        # @TODO override port if URI formatted host contains port.
        self.port = port

    def __check_connectivity(self):
        """Returns True if host:port accepts connections."""
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.settimeout(5)
            s.connect((self.host, int(self.port)))
            s.close()
            return True

        except (socket.timeout, socket.error) as e:
            """
            catches the two exception types I've seen in testing.
            tested by setting host to localhost (connection refused),
            8.8.8.8 (request timed out)
            """
            e = str(e).split('] ')[-1]
            raise RuntimeError(
                '\033[37;1m{0}.{1}: \033[31m{2}\033[0m.'.format(self.host, self.port, e))

    def __create_proxy(self):
        """Returns a proxy object used to interact with the Freeswitch API"""
        try:
            server = ServerProxy("http://{0}@{1}:{2}".format(self.auth,
                                                             self.host,
                                                             self.port))
            return server
        # @TODO to find out what exceptions ServerProxy throws.
        except Exception as e:
            raise e

    def connect(self, attempts):
        for n in range(int(attempts)):
            try:
                self.__check_connectivity()
                server = self.__create_proxy()
                server.system.methodExist('freeswitch.api')
                print "Connection attempt", n + 1, "successful"
                return server
            except Exception as e:
                if n < int(attempts):
                    print "Connection attempt", n + 1, e
                    pass
                else:
                    raise RuntimeError(e)

    def __check_destination(self, destination):
        """checks destination type. Returns either ptsn or uri or None."""
        ptsn_pattern = r'^\+?\d+$'
        uri_pattern = r'^(sip:)?.+@.+'
        re_ptsn_pattern = re.compile(ptsn_pattern)
        re_uri_pattern = re.compile(uri_pattern)
        if re_ptsn_pattern.search(destination):
            return 'ptsn'
        elif re_uri_pattern.search(destination):
            return 'uri'
        else:
            return

    def __format_command_string(self, call):
        """Creates a Freeswitch friendly command string from Call variables."""
        if self.__check_destination(call.destination) == 'ptsn':
            external_option = call.external_gateway
            if call.prefix:
                prefix = re.split('\*|#', call.prefix)[0]
            else:
                prefix = ''
        elif self.__check_destination(call.destination) == 'uri':
            external_option = call.external_profile
            prefix = ''
        command_string = (
            'originate {{fax_ident=\'Test Fax\',fax_header=\'Test Fax\','
            'api_hangup_hook=\'system /bin/grep {0} /usr/local/freeswitch/log/'
            'freeswitch.log > /var/tmp/fslog/{0}.log\',origination_uuid={0},'
            'fax_disable_v17={1},fax_use_ecm={2},origination_caller_id_number='
            '{3},fax_verbose=true,fax_enable_t38={4},ignore_early_media=true,'
            'fax_enable_t38_request={5},absolute_codec_string={6}}}{7}{8}*{9} '
            '&txfax(/usr/local/freeswitch/fax/{10})').format(
                call.oid, call.v17, call.ecm, call.source, call.t38,
                call.t38_request, call.codec, external_option, prefix,
                call.destination, call.file)
        return command_string

    def __check_status(self, server, oid):
        """
        Blocking function checks freeswitch for call status.
        Currently checks once per second and is used for timestamps.
        """
        time.sleep(2)
        while 1:
            callstatus = server.freeswitch.api('uuid_exists', oid)
            if callstatus == 'true':
                time.sleep(1)
                pass
            else:
                break

    def send_call(self, server, call):
        """
        API requirements: external_profile + URI
                            external_gateway + prefix + DID
        """
        command_string = self.__format_command_string(call)
        try:
            starttime = time.time()
            server.freeswitch.api('bgapi', command_string)
            self.__check_status(server, call.oid)
            endtime = time.time()
            print "{0} completed successfully in {1} seconds.".format(
                call.oid,
                str(endtime - starttime))
        except AttributeError as e:
            if 'freeswitch' in str(e):
                raise RuntimeError(
                    'Invalid server object passed to send_call.')

