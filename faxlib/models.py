# -*- coding: utf-8 -*-
import uuid


class Call(object):
    """
    Attributes:
        source      (string): Phone number in E.164 format.
        destination (string): Phone number in E.164 or sip URI
        oid      (string): A unique id for use in call control. Not call-id.
        codec       (string): Set initial call codec.       Default: 'PCMU'
        prefix      (int): Set tech prefix for IP auth

    """

    def __init__(self, **kwargs):
        # Set all of the parameters to their default values
        self.source = None
        self.destination = None
        self.oid = str(uuid.uuid1())
        self.codec = 'PCMU'
        self.prefix = ''
        self.external_profile = 'sofia/external/'
        self.external_gateway = 'sofia/gateway/flowroute/'

        # Create a mapping from API property names to Model property names
        replace_names = {
            "source": "source",
            "destination": "destination",
            "oid": "oid",
            "codec": "codec",
            "prefix": "prefix",
            "external_gateway": "external_gateway",
            "external_profile": "external_profile"
        }

        # Parse all of the Key-Value arguments
        if kwargs is not None:
            for key in kwargs:
                # Only add arguments that are actually part of this object
                if key in replace_names:
                    setattr(self, replace_names[key], kwargs[key])


class Fax(Call):
    """
    Attributes:
        source      (string): Phone number in E.164 format.
        destination (string): Phone number in E.164 or sip URI
        oid      (string): A unique id for the call
        codec       (string): Set initial call codec.       Default: 'PCMU'
        prefix      (int): Set tech prefix for IP auth
        t38         (bool): Send as T.38.                   Default: True
        t38_request (bool): Send T.38 reINVITE on outbound. Default: True
        v17         (bool): Send as T.38 v17.               Default: False
        ecm         (bool): Enable ECM.                     Default: False
        size        (str): small or large fax.              Default: 'small'

    """
    def __init__(self, **kwargs):
        super(Fax, self).__init__(**kwargs)
        self.t38 = True
        self.t38_request = True
        self.v17 = False
        self.ecm = False
        self.file = 'testfax_stdres.tif'

        # Create a mapping from API property names to Model property names
        replace_names = {
            "v17": "v17",
            "t38": "t38",
            "t38_request": "t38_request",
            "ecm": "ecm",
            "file": "file"
        }

        # Parse all of the Key-Value arguments
        if kwargs is not None:
            for key in kwargs:
                # Only add arguments that are actually part of this object
                if key in replace_names:
                    setattr(self, replace_names[key], kwargs[key])
